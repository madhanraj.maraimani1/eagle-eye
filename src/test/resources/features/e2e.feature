Feature: To test end to end journey

  Scenario: Offer Creation and Verification
    Given I create an offer in Eagle Eye
    Then I should see PreProcessOffer Service Esb logs in Splunk within 60 seconds
    And I should see Publish Offer ONPS logs in Splunk within 5 minutes
    And I should see PublishOffer Service Esb logs in Splunk
    Then I verify the offer batch details are present in DDS
    And I verify the offer batch details are present in CDS
    Then I Login to ONPS UI
