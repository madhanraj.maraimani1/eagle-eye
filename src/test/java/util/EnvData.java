package util;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class EnvData {

    private static DocumentContext ENV_JSON_DATA;

    private static String ENV;

    public static void load(String env) {
        ENV = env;
        try {
            Path path = Paths.get(EnvData.class.getClassLoader()
                    .getResource("env.json").toURI());
            ENV_JSON_DATA = JsonPath.parse(new String(Files.readAllBytes(path)));
        } catch (URISyntaxException | IOException e) {
            e.printStackTrace();
        }
    }

    public static String getData(String path) {
        return ENV_JSON_DATA.read(String.format("$.%s.%s", ENV, path));
    }

}
