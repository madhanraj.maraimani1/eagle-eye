package pages.splunk;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.Keys;

import java.time.Duration;
import java.util.Arrays;

import static com.codeborne.selenide.CollectionCondition.sizeGreaterThanOrEqual;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static util.EnvData.getData;

public class SplunkSearchPage {

    public SplunkSearchPage open() {
        Selenide.open(getData("splunk.url"));
        loginWithNetDom();
        $(".search-searchhistory").waitUntil(visible, 30000);
        return this;
    }

    private void loginWithNetDom() {
        if ($("#jlp-logo").exists()) {
            $("#username").val(getData("splunk.username"));
            $("#password").val(getData("splunk.password"));
            $(".ping-button.allow").click();
        }
    }

    public void search(String searchIndex, int maxSearchTimeInSecs, String... textsToSearch) {
        new AceEditor().clearContent().sendContent(searchIndex);

        SelenideElement searchButton = $(".search-button");

        SelenideElement stopSearchButton = $("a.stop.btn-pill");
        stopSearchButton.waitUntil(cssClass("disabled"), 10000);

        SelenideElement searchRangeDropDown = $(".search-timerange a");
        searchRangeDropDown.click();

        SelenideElement selectTodaySearch = $("a[data-earliest='@d']");
        selectTodaySearch.shouldBe(visible).click();

        Runnable searchAndCheck = () -> {
            searchButton.click();
            stopSearchButton.waitUntil(cssClass("disabled"), 10000);
            ElementsCollection searchResults = $$("table.events-results-table[id] div.raw-event").shouldHave(sizeGreaterThanOrEqual(1));
            for (String textToSearch : textsToSearch) {
                searchResults.find(text(textToSearch)).should(exist);
            }
        };

        Wait().withTimeout(Duration.ofSeconds(maxSearchTimeInSecs))
                .withMessage("Search for given Texts - " + Arrays.toString(textsToSearch))
                .until((d) -> {
                    try {
                        searchAndCheck.run();
                        return true;
                    } catch (Throwable ex) {
                        return false;
                    }
                });
    }

    public String getTimeStamp(String textContent) {
        return $$("table.events-results-table[id] div.raw-event")
                .find(text(textContent))
                .parent()
                .closest("tr")
                .find("span.formated-time")
                .attr("data-time-iso");
    }

    class AceEditor {

        SelenideElement aceLayerTextArea = $(".ace_text-layer .ace_line");

        SelenideElement aceInputTextArea = $("textarea.ace_text-input");

        SelenideElement aceEditor = $(".ace_content");

        public AceEditor clearContent() {
            aceLayerTextArea.waitUntil(visible, 5000);
            executeJavaScript(String.format("ace.edit('%s').setValue('');", aceEditor.attr("id")));
            return this;
        }

        public AceEditor insertContent(String cssText) {
            aceLayerTextArea.waitUntil(visible, 5000);
            executeJavaScript(String.format("ace.edit('%s').navigateFileEnd();", aceEditor.attr("id")));
            sendContent(cssText);
            return this;
        }

        private AceEditor sendContent(String cssText) {
            aceLayerTextArea.waitUntil(visible, 5000);
            aceInputTextArea.sendKeys(cssText);
            aceInputTextArea.sendKeys(Keys.ENTER);
            return this;
        }

        public String getContent() {
            aceLayerTextArea.waitUntil(visible, 5000);
            return executeJavaScript(String.format("ace.edit('%s').getSession().getValue();", aceEditor.attr("id")));
        }
    }

}
