package pages.cds;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;

import static com.codeborne.selenide.CollectionCondition.size;
import static com.codeborne.selenide.CollectionCondition.textsInAnyOrder;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static util.EnvData.getData;

public class IndexPage {

    public IndexPage open() {
        Selenide.open(getData("cds.url"));
        $("h1").shouldHave(text("Index of "));
        return this;
    }

    public void verifyBranchData(String date, String time) {
        ElementsCollection fileLinks = $$("img[alt]+a");
        fileLinks.find(text(date)).click();
        fileLinks.find(text("MIS")).click();

        String xmlNaming = "INCENTIVE UPDATE_" + date + "-" + time;
        fileLinks.filter(text(xmlNaming))
                .shouldHave(size(3), textsInAnyOrder("_1.xml", "_1_EOB.xml", "_1_EOP.xml"));
    }

}
