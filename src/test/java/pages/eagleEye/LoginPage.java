package pages.eagleEye;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static util.EnvData.getData;

public class LoginPage {

    public LoginPage() {
        $("#login").should(visible);
    }

    public DashboardPage login() {
        $("#el-username").val(getData("eagleEye.username"));
        $("#el-password").val(getData("eagleEye.password"));
        $("#submitbutton").click();
        $("#user-settings-dropdown").should(visible);
        return new DashboardPage();
    }
}
