package pages.eagleEye;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import model.CampaignData;
import org.junit.Assert;

import java.util.Optional;
import java.util.function.Consumer;

import static com.codeborne.selenide.CollectionCondition.texts;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static org.hamcrest.CoreMatchers.containsString;

public class NewCampaignPage extends CommonPage {

    CampaignData campaignData;

    public NewCampaignPage() {
        Assert.assertThat("Title Should be Campaign Create", title(), containsString("Campaigns"));
        $(".page-header h2")
                .shouldBe(visible)
                .shouldHave(text(" Select Type for New Campaign"));
        $$("#campaign-types span").shouldHave(texts(
                "Waitrose Discount Basket",
                "Waitrose Discount Product (Generic)",
                "Waitrose Discount Product (Loyalty)",
                "Waitrose Combo (Generic)",
                "Waitrose Combo (Loyalty)"
        ));
    }

    public NewCampaignPage withCampaignData(CampaignData campaignData) {
        this.campaignData = campaignData;
        return this;
    }

    private void checkActiveSection(String sectionName) {
        $("[data-section='" + sectionName + "']")
                .shouldHave(attribute("data-status", "active"));
    }

    private void selectFromDropDown(Condition condition, String textToSelect) {
        $$("select").find(condition).parent().find(".select2-container").click();
        $$(".select2-results__options[aria-expanded=true] li")
                .find(text(textToSelect))
                .shouldBe(visible)
                .click();
    }

    private void selectFromDropDown(SelenideElement selectElement, String textToSelect) {
        selectElement.parent().find(".select2-container").click();
        $$(".select2-results__options[aria-expanded=true] li")
                .find(text(textToSelect))
                .shouldBe(visible)
                .click();
    }

    private void searchAndSelect(Condition condition, String textToSearch) {
        SelenideElement selectContainer = $$("select").find(condition).parent().find(".select2-container");
        selectContainer.click();
        selectContainer.find(".select2-search__field").sendKeys(textToSearch);
        $$(".select2-results__options[aria-expanded=true] li")
                .find(text(textToSearch))
                .shouldBe(visible)
                .click();
    }

    private void clickNext() {
        $("[data-action='next']").click();
    }

    public NewCampaignPage selectCampaign(String type) {
        $$("#campaign-types span").find(text(type)).click();
        return this;
    }

    public NewCampaignPage enterGeneralDetail() {
        CampaignData.GeneralData generalData = campaignData.getGeneralData();
        checkActiveSection("general");

        $("#campaign-campaignName-input").val(generalData.getCampaignName());
        $("#campaign-properties-campaign-v2-details-printerMessage-input").val(generalData.getPrinterMessage());
        selectFromDropDown(name("campaign[campaignMode]"), generalData.getMode());
        $("#campaign-date-start").val(generalData.getStartDate());
        $("#campaign-date-end").val(generalData.getEndDate());
        generalData.getTags()
                .forEach(tagName -> searchAndSelect(id("campaign-tags"), tagName));

        clickNext();
        return this;
    }

    public NewCampaignPage enterCreationDetail() {
        CampaignData.CreationData creationData = campaignData.getCreationData();
        checkActiveSection("creation");

        searchAndSelect(id("redemption-partners-selector"), creationData.getRedemptionPartners());

        clickNext();
        return this;
    }

    public NewCampaignPage enterQualificationDetail() {
        CampaignData.QualificationData qualificationData = campaignData.getQualificationData();
        checkActiveSection("qualification");

        $("#campaign-properties-campaign-v2-offer-qualification-basket-minimumBasketSpend-input").val(qualificationData.getBasketSpend());

        Consumer<String> pickStore = (store) -> {
            $(".picker-search-value").shouldBe(visible)
                    .val(store)
                    .pressEnter();
            $(".search-results .grid-results li").shouldBe(visible).click();
            $("button[data-control-button='add-selected']").click();
            $(".selected-results .grid-results li").shouldBe(visible);
            $("[id*=store-selector] button[data-save]").click();
        };

        $("button[data-type=store][data-input*='included']").click();
        pickStore.accept(qualificationData.getIncludeStores());

        $("button[data-type=store][data-input*='excluded']").click();
        pickStore.accept(qualificationData.getExcludeStores());

        Optional.ofNullable(qualificationData.getIncludeStoreTags())
                .ifPresent(includeTags -> {
                    searchAndSelect(attribute("title", "Select store tags for inclusion"), includeTags);
                });

        Optional.ofNullable(qualificationData.getExcludeStoreTags())
                .ifPresent(excludeTags -> {
                    searchAndSelect(attribute("title", "Select store tags for exclusion"), excludeTags);
                });

        clickNext();
        return this;
    }

    public NewCampaignPage enterRewardDetail() {
        CampaignData.RewardData rewardData = campaignData.getRewardData();

        checkActiveSection("reward");

        $("#campaign-properties-campaign-v2-offer-reward-standard-value-discountAmount-input").val(rewardData.getDiscountFixedPrice());
        clickNext();
        return this;
    }

    public NewCampaignPage handleWaitroseGeneric() {
        CampaignData.WaitroseGenericData waitroseGenericData = campaignData.getWaitroseGenericData();

        checkActiveSection("waitroseGeneric-201");

        selectFromDropDown($("select[name*=campaignType]"), waitroseGenericData.getCampaignType());
        selectFromDropDown($("select[name*=loyaltyCardRequired]"), waitroseGenericData.getLoyaltyCard());
        selectFromDropDown($("select[name*=bulkEntry]"), waitroseGenericData.getBulkEntry());
        selectFromDropDown($("select[name*=staffDiscount]"), waitroseGenericData.getStaffDiscount());
        selectFromDropDown($("select[name*=allowPriceOverrideItems]"), waitroseGenericData.getPriceOverride());
        $("[id*=waitroseCampaignCode").val(waitroseGenericData.getCampaignCode());
        $("[id*=waitroseElementCode]").val(waitroseGenericData.getElementCode());

        clickNext();
        return this;
    }

    public NewCampaignPage handleIncentiveType() {
        checkActiveSection("incentiveType-164");

        clickNext();
        return this;
    }

    public NewCampaignPage enterBarcode() {
        checkActiveSection("barcode");

        clickNext();
        return this;
    }

    public NewCampaignPage verifySummary() {
        checkActiveSection("summary");
        $("[title = 'Approve Campaign']").click();
        $("[data-action='create']").click();
        return this;
    }

    public String getCampaignCode() {
        return $("#creator-success-msg h4")
                .waitUntil(visible, 5000)
                .text()
                .replace("Successfully created new campaign (", "")
                .replace(")", "");
    }

}
