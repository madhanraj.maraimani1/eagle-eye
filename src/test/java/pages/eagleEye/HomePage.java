package pages.eagleEye;

import com.codeborne.selenide.Selenide;
import org.junit.Assert;

import static com.codeborne.selenide.Selenide.title;
import static util.EnvData.getData;

public class HomePage extends CommonPage {

    public LoginPage open() {
        Selenide.open(getData("eagleEye.url"));
        Assert.assertEquals("Title Check", "Eagle Eye AIR", title());
        return new LoginPage();
    }


}
