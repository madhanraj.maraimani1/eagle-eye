package pages.eagleEye;

import com.codeborne.selenide.SelenideElement;
import org.junit.Assert;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.title;
import static org.hamcrest.CoreMatchers.containsString;

public class DashboardPage extends CommonPage {

    public DashboardPage() {
        Assert.assertThat("Title Should be Dashboard", title(), containsString("Dashboard"));
        $(".page-header h2")
                .shouldBe(visible)
                .shouldHave(text("Dashboard"));
    }

    public static SelenideElement getSubMenuLink() {
        return null;
    }

    public static SelenideElement getMenuLink() {
        return null;
    }
}
