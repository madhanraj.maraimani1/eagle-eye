package pages.eagleEye;

import com.codeborne.selenide.SelenideElement;

import java.util.Optional;

import static com.codeborne.selenide.Selenide.$;

public abstract class CommonPage {

    public void navigateTo(SelenideElement menuLink, SelenideElement subMenuLink) {
        String className = menuLink.attr("class");
        if (className.contains("accordion-toggle")) {
            if (className.contains("collapsed")) {
                menuLink.click();
            }
        } else {
            menuLink.click();
        }
        Optional.ofNullable(subMenuLink).ifPresent(SelenideElement::click);
    }

    public NewCampaignPage navigateToNewCampaignPage() {
        navigateTo($("#menu-campaign-link"), $("#menu-createcampaign-link"));
        return new NewCampaignPage();
    }
}
