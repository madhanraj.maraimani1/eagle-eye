package pages.onps;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static util.EnvData.getData;

public class LoginPage {

    public LoginPage() {
        $(".ui-panel-title").should(visible).should(text("Logon"));
    }

    public HomePage login() {
        $("#j_username").val(getData("onps.username"));
        $("#j_password").val(getData("onps.password"));
        $("#submit").click();
        return new HomePage();
    }
}
