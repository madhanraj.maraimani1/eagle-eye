package pages.onps;

import com.codeborne.selenide.Selenide;
import org.junit.Assert;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.title;
import static util.EnvData.getData;

public class HomePage {

    public LoginPage open() {
        Selenide.open(getData("onps.url"));
        Assert.assertEquals("Title Check", "ONPS", title());
        return new LoginPage();
    }

    public void verifyHomePage() {
        $x("//h3").shouldHave(text("Welcome to ONPS"));
    }
}
