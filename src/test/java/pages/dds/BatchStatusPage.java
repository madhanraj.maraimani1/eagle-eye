package pages.dds;

import com.codeborne.selenide.ElementsCollection;
import org.junit.Assert;

import static com.codeborne.selenide.Condition.or;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$$x;
import static com.codeborne.selenide.Selenide.title;

public class BatchStatusPage {

    public BatchStatusPage() {
        Assert.assertEquals("Check title of page", "Status of Imported batches", title());
    }

    public void checkBatch(String dateTime) {
        ElementsCollection dateCellElements =
                $$x("//table//tr[@bgcolor]/td[4]")
                        .filter(text(dateTime))
                        .shouldHaveSize(2);
        dateCellElements.forEach(dateCell -> {
            dateCell.parent()
                    .$x("./td[5]")
                    .should(or("Have Success Status",
                            text("Processed - No Errors"),
                            text("Has been Distributed")));
        });
    }
}
