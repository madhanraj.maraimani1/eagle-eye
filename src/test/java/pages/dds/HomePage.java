package pages.dds;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;

import static com.codeborne.selenide.Selenide.$;
import static util.EnvData.getData;

public class HomePage {

    public HomePage open() {
        Selenide.open(getData("dds.url"));
        $("h1").should(Condition.text("Data Distribution Reporting"));
        return this;
    }

    public BatchStatusPage goToBatchStatus() {
        $("[name=searchBatch]").click();
        return new BatchStatusPage();
    }

}
