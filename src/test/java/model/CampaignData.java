package model;

import lombok.Builder;
import lombok.Data;
import lombok.Singular;

import java.util.List;

@Data
@Builder
public class CampaignData {

    GeneralData generalData;

    CreationData creationData;

    QualificationData qualificationData;

    RewardData rewardData;

    WaitroseGenericData waitroseGenericData;

    IncentiveData incentiveData;

    BarcodeData barcodeData;

    @Builder
    @Data
    public static class GeneralData {
        String campaignName;
        String mode;
        String printerMessage;
        String startDate;
        String endDate;
        @Singular
        List<String> tags;
    }

    @Builder
    @Data
    public static class CreationData {
        String couponExpiry;
        String redemptionPartners;
        String fixedExpiryDate;
        String expiryHours;
        String expiryDays;
    }

    @Builder
    @Data
    public static class QualificationData {
        String basketSpend;
        String includeStoreTags;
        String excludeStoreTags;
        String includeStores;
        String excludeStores;
    }

    @Builder
    @Data
    public static class RewardData {
        String discountFixedPrice;
        String discountPercent;
        String triggerLimit;
    }

    @Builder
    @Data
    public static class WaitroseGenericData {
        String campaignType;
        @Builder.Default
        String loyaltyCard = "No";
        @Builder.Default
        String bulkEntry = "No";
        @Builder.Default
        String staffDiscount = "No";
        @Builder.Default
        String priceOverride = "Yes";
        String campaignCode;
        String elementCode;
    }

    @Builder
    @Data
    public static class IncentiveData {
    }

    @Builder
    @Data
    public static class BarcodeData {
    }
}
