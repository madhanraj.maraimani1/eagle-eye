package modules.eagleEye;

import model.CampaignData;
import model.CampaignData.*;
import pages.eagleEye.HomePage;
import pages.eagleEye.NewCampaignPage;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Campaign {

    private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

    public enum CampaignType {
        WAITROSE_DISCOUNT_BASKET("Waitrose Discount Basket"),
        WAITROSE_DISCOUNT_PRODUCT_GENERIC("Waitrose Discount Product (Generic)"),
        WAITROSE_DISCOUNT_PRODUCT_LOYALTY("Waitrose Discount Product (Loyalty)"),
        WAITROSE_COMBO_GENERIC("Waitrose Combo (Generic)"),
        WAITROSE_COMBO_LOYALTY("Waitrose Combo (Loyalty)");

        String text;

        CampaignType(String text) {
            this.text = text;
        }

        public String getText() {
            return text;
        }
    }

    public String createCampaign(CampaignType campaignType) {

        CampaignData campaignData = CampaignData.builder()
                .generalData(GeneralData.builder()
                        .campaignName("Campaign VOUCHER")
                        .mode("Open")
                        .startDate(LocalDateTime.now().format(DATE_FORMAT))
                        .endDate(LocalDateTime.now().plusDays(30).format(DATE_FORMAT))
                        .printerMessage("Test WR Coupon/Voucher")
                        .tag("Sujeetha")
                        .tag("Test")
                        .build())
                .creationData(CreationData.builder()
                        .couponExpiry("Campaign Expiry")
                        .redemptionPartners("Waitrose Instore")
                        .build())
                .qualificationData(QualificationData.builder()
                        .basketSpend("600")
                        .includeStores("257 Jubilee House")
                        .excludeStores("262 Jubilee House")
                        .build())
                .rewardData(RewardData.builder()
                        .discountFixedPrice("85")
                        .discountPercent("10")
                        .triggerLimit("1")
                        .build())
                .waitroseGenericData(WaitroseGenericData.builder()
                        .campaignType("Just for you")
                        .loyaltyCard("No")
                        .bulkEntry("No")
                        .staffDiscount("No")
                        .priceOverride("Yes")
                        .campaignCode("MIC200002")
                        .elementCode("WW202020")
                        .build())
                .incentiveData(IncentiveData.builder().build())
                .build();

        NewCampaignPage newCampaignPage = new HomePage()
                .open()
                .login()
                .navigateToNewCampaignPage()
                .selectCampaign(campaignType.getText())
                .withCampaignData(campaignData);

        return newCampaignPage.enterGeneralDetail()
                .enterCreationDetail()
                .enterQualificationDetail()
                .enterRewardDetail()
                .handleWaitroseGeneric()
                .handleIncentiveType()
                .enterBarcode()
                .verifySummary()
                .getCampaignCode();
    }
}
