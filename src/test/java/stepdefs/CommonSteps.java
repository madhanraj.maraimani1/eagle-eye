package stepdefs;

import com.codeborne.selenide.logevents.SelenideLogger;
import io.cucumber.java8.En;
import io.qameta.allure.selenide.AllureSelenide;
import util.EnvData;

public class CommonSteps implements En {
    public CommonSteps() {
        Before(hook -> {
            EnvData.load(System.getProperty("runEnv"));
            SelenideLogger.addListener("AllureSelenide",
                    new AllureSelenide()
                            .screenshots(true)
                            .savePageSource(false));
        });
    }
}