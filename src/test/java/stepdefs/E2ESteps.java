package stepdefs;


import io.cucumber.java8.En;
import modules.eagleEye.Campaign;
import pages.splunk.SplunkSearchPage;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static modules.eagleEye.Campaign.CampaignType.WAITROSE_DISCOUNT_BASKET;

public class E2ESteps implements En {

    String campaignId;

    Date offerCreatedDate;

    public E2ESteps() {

        Given("^I create an offer in Eagle Eye$", () -> createOffer());

        Then("I should see PreProcessOffer Service Esb logs in Splunk within {int} seconds", (Integer maxSearchSeconds) -> {
            String esbIndex = "index=wtr_esb_sit source=\"/opt/webMethods/wM10/IntegrationServer/instances/sit13is01/logs/server.log\" " + campaignId;
            String verifyEagleEye = "PreProcessOffer_EE v1.0.0 -- Successfully received the offer data from Eagle Eye and published to UMQ_WSB_EE_OFFER queue";
            String verifyONPS = "PreProcessOffer_SUB_ONPS v1.0.0 -- Message has been successfully sent to ONPS";
            searchSplunkLog(esbIndex, maxSearchSeconds, verifyEagleEye, verifyONPS);
        });

        And("I should see Publish Offer ONPS logs in Splunk within {int} minutes", (Integer maxSearchMinutes) -> {
            String onpsIndex = "index=wtr_onps_sit host=ukwka1-00002220 " + campaignId;
            String verificationText = "PublishedWithCampaignId=\"" + campaignId + "\"";
            searchSplunkLog(onpsIndex, maxSearchMinutes * 60, verificationText);
        });

        And("I should see PublishOffer Service Esb logs in Splunk", () -> {
            String esbIndex = "index=wtr_esb_sit source=\"/opt/webMethods/wM10/IntegrationServer/instances/sit13is01/logs/server.log\" " + campaignId;
            String campaignMessage = "PublishOffer v1.0.0 -- Offer message with CampaignId: " + campaignId;
            String cdsMessage = "PublishOffer_SUB_VBS v1.0.0 -- Successfully send XML1, EOB and EOP to CDS Queue";
            String posMessage = "PublishOffer_SUB_VBS v1.0.0 -- Message [";
            searchSplunkLog(esbIndex, 10, campaignMessage, cdsMessage, posMessage);
            offerCreatedDate = getOfferCreatedDateFromSplunk(campaignMessage);
        });

        Then("I verify the offer batch details are present in DDS", () -> verifyBatchInDDS());

        And("I verify the offer batch details are present in CDS", () -> verifyXmlInCds());

        Then("I Login to ONPS UI", () -> loginToONPS());

    }

    private Date getOfferCreatedDateFromSplunk(String verifyONPS) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        String isoTimeStamp = new SplunkSearchPage().getTimeStamp(verifyONPS);
        try {
            return sdf.parse(isoTimeStamp);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void loginToONPS() {
        new pages.onps
                .HomePage()
                .open()
                .login()
                .verifyHomePage();
    }

    private void createOffer() {
        campaignId = new Campaign().createCampaign(WAITROSE_DISCOUNT_BASKET);
    }

    private void searchSplunkLog(String searchIndex, int maxSearchSecs, String... verificationTexts) {
        new SplunkSearchPage()
                .open()
                .search(searchIndex, maxSearchSecs, verificationTexts);
    }

    private void verifyBatchInDDS() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        new pages.dds.HomePage()
                .open()
                .goToBatchStatus()
                .checkBatch(simpleDateFormat.format(offerCreatedDate));
    }

    private void verifyXmlInCds() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH.mm");
        String dateTime = simpleDateFormat.format(offerCreatedDate);
        String date = dateTime.split(" ")[0];
        String time = dateTime.split(" ")[1];
        new pages.cds.IndexPage().open().verifyBranchData(date, time);
    }
}
